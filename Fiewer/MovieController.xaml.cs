﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Fiewer
{
    public partial class MovieController : UserControl
    {
        #region Fields
        internal List<MovieWindow> MovieWindowList;
        private DispatcherTimer SeekSliderTimer;
        //        public bool isFingerPlay = false; // 指着目再生機能を使用中かどうか
        //        public int[] noteNums; // 着目している指で演奏しているノートナンバー
        public bool isExpMode;
        #endregion

        #region Constructor
        public MovieController()
        {
            InitializeComponent();
            this.seekSlider.AddHandler(Slider.MouseLeftButtonUpEvent, new MouseButtonEventHandler(seekSlider_MouseButtonLeftUp), true);
            this.seekSlider.AddHandler(Slider.MouseLeftButtonDownEvent, new MouseButtonEventHandler(seekSlider_MouseButtonLeftDown), true);
            this.playSpeedSlider.AddHandler(Slider.MouseLeftButtonUpEvent, new MouseButtonEventHandler(playSpeedSlider_MouseLeftButtonUp), true);
            this.SetSeekSliderTimer();
        }
        #endregion

        #region change Media Element Play Speed
        /// <summary>
        /// 再生速度調整スライダーを移動し終わったとき(左クリックを離したとき)
        /// mediaElementの再生速度をスライダーの値から計算して設定する
        /// </summary>
        private void playSpeedSlider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // 元の再生速度を100として速度の増減を計算
            double sp = this.playSpeedSlider.Value / 100;
            new Log("Play Speed Changed", "slider's value", sp.ToString());
            if (this.MovieWindowList != null)
            {
                foreach (MovieWindow mw in this.MovieWindowList)
                {
                    mw.changeMediaElementSpeedRatio(sp);
                }
            }
        }
        #endregion

        #region Play Keystroke Scene
        public void playKeystrokeScene(int noteNo)
        {
            foreach (MovieWindow mw in this.MovieWindowList)
            {
                mw.PlayKeystrokeScene(noteNo);
            }
        }
        #endregion

        #region Frame by Frame

        /// <summary>
        /// クリックされたら動画の再生時刻を25msec戻す
        /// </summary>
        private void FramebyFrameButton_Click(object sender, RoutedEventArgs e)
        {
            int tag = new Calc().ButtonTag2Int(sender);
            if(tag < 0)
                new Log("prevFrameButton Clicked");
            else new Log("nextFrameButton Clicked");

            foreach (MovieWindow mw in MovieWindowList)
            {
                mw.FramebyFrame(tag);
            }
        }
        #endregion

        #region Seek Bar Control
        /// <summary>
        /// SeekSliderの最大値をMediaElementに合わせる
        /// </summary>
        public void SetSeekSlider()
        {
            Midi[] midi = this.GetLongestMediaElement(this.MovieWindowList);
            double max = (isExpMode)? midi[24].touchPlayTimeFront - midi[1].touchPlayTimeFront : midi[96].touchPlayTimeFront - midi[1].touchPlayTimeFront;
            this.seekSlider.Maximum = max;
            this.seekSlider.Width = (this.isExpMode)? this.seekSlider.Maximum *50 : this.seekSlider.Maximum * 5;
        }

        /// <summary>
        /// シークバーの長さを合わせる最も再生時間が長い動画のMIDIを取得する
        /// </summary>
        private Midi[] GetLongestMediaElement(List<MovieWindow> mwl)
        {
            Midi[] midi = null;
            foreach(MovieWindow mw in mwl)
            {
                if (midi == null)
                    midi = mw.pianist.midis;
                else if (midi[96].touchPlayTimeFront < mw.pianist.midis[96].touchPlayTimeFront)
                    midi = mw.pianist.midis;
            }
            return midi;
        }

        /// <summary>
        /// シークバーのスライド開始時（左クリック押したとき）
        /// </summary>
        private void seekSlider_MouseButtonLeftDown(object sender, MouseButtonEventArgs e)
        {
            new Log("Slide seekSlider","from", (sender as Slider).Value.ToString() + "%");
            foreach(MovieWindow mw in this.MovieWindowList)
            {
                mw.Pause();
            }

            if (this.SeekSliderTimer.IsEnabled)
                this.SeekSliderTimer.Stop();
            

        }

        /// <summary>
        /// 動画の再生位置を変える
        /// </summary>
        private void seekSlider_MouseButtonLeftUp(object sender, MouseButtonEventArgs e)
        {
            int val = (int)this.seekSlider.Value;
            new Log("Slide seekSlider", "to", val.ToString() + "%");

            foreach (MovieWindow mw in this.MovieWindowList)
            {
                // シークバーと合致する動画の再生時刻
                Midi midi = mw.pianist.midis[1];
                float f = val + midi.touchPlayTimeFront;
                float l = f + midi.Sub_FL;
                float r = f + midi.Sub_FR;

                // Positionを移動
                mw.mediaElement_front.Position = new TimeSpan(0, 0, 0, (int)f, (int)((f % 1) * 1000));
                mw.mediaElement_left.Position = new TimeSpan(0, 0, 0, (int)l, (int)((l % 1) * 1000));
                mw.mediaElement_right.Position = new TimeSpan(0, 0, 0, (int)r, (int)((r % 1) * 1000));
            }

            foreach (MovieWindow mw in this.MovieWindowList)
            {
                mw.PlayContinue();
            }
            this.SeekSliderTimer.Start();
        }

        public void SetSeekSliderTimer()
        {
            this.SeekSliderTimer = new System.Windows.Threading.DispatcherTimer();
            SeekSliderTimer.Tick += new EventHandler(SeekSliderTimer_Tick);
            SeekSliderTimer.Interval = new TimeSpan(0, 0, 1);
            SeekSliderTimer.Start();
        }
        /// <summary>
        /// SeekSliderのつまみの位置をMediaElementに合わせる
        /// 一定時間ごとに呼び出される
        /// </summary>
        private void SeekSliderTimer_Tick(object sender, EventArgs e)
        {
            if (this.MovieWindowList.Count > 0)
            {
                TimeSpan pos = this.MovieWindowList[0].MediaElementDic.Values.ToArray()[0].Position;
                this.seekSlider.Value = pos.Seconds + pos.Minutes * 60 - this.MovieWindowList[0].pianist.midis[1].touchPlayTimeFront;
                if(this.isExpMode && pos.Seconds > this.MovieWindowList[0].pianist.midis[25].touchPlayTimeFront)
                    foreach(MovieWindow mw in this.MovieWindowList)
                    {
                        mw.Pause();
                    }

            }
        }
        #endregion

        #region Pause and Play From Continue
        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            Log log = new Log(true);

            foreach (MovieWindow mw in this.MovieWindowList)
            {
                mw.Pause();
                foreach (MediaElement val in mw.MediaElementDic.Values)
                {
                    log.Values.Add(mw.Title + "'s " + val.Name + "'s Position", val.Position.ToString());
                }
            }
            log.Write("Pause Button Clicked", log.Values);
        }

        private void PlayContinueButton_Click(object sender, RoutedEventArgs e)
        {
            new Log("Play Continue Button Clicked");
            foreach (MovieWindow mw in this.MovieWindowList)
            {
                mw.PlayContinue(); 
            }

        }
        #endregion

        #region Show MediaElement Check Box
        /// <summary>
        /// チェックボックスにチェックを付けたときに呼び出される
        /// チェックを付けたチェックボックスに対応するMediaElementを表示する
        /// 対応はValue.resxを参照
        /// </summary>
        private void MediaCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            int tag = new Calc().CheckBoxTag2Int(sender);
            new Log("Media Open Checked", new string[] { "CheckBox's Tag" }, new string[] { tag.ToString() });

            // 起動時以外かつMovieWindowが表示されているときの呼び出しなら
            if (this.MovieWindowList != null　&& this.MovieWindowList.Count > 0) {                             
            foreach(MovieWindow mw in this.MovieWindowList)
            {
                    mw.ShowMediaElement(tag.ToString());
            }
            }
        }

        /// <summary>
        /// チェックを外したチェックボックスに対応するMediaElementを非表示にする
        /// 対応はValue.resxを参照
        /// </summary>
        private void MediaCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            int tag = new Calc().CheckBoxTag2Int(sender);
            new Log("Media Open Unchecked", new string[] { "CheckBox's Tag" }, new string[] { tag.ToString() });

            // MovieWindowが表示されているときの呼び出しなら
            if (this.MovieWindowList.Count > 0)
            {
                foreach (MovieWindow mw in this.MovieWindowList)
                {
                    mw.HideMediaElement(tag.ToString());
                    
                }
            }
        }

        #endregion


        #region Finger focused play

        /// <summary>
        ///　指番号ボタンをクリックされたら呼び出される
        ///　クリックされた指番号ボタンに対応するシーンを連続で再生する
        /// </summary>
        private void finger_Click(object sender, RoutedEventArgs e)
        {
/*            this.isFingerPlay = true;

            // クリックしたボタンのTagを取り出す
            var button = sender as Button;
            int fingerNo = Convert.ToInt32(button.Tag);
            new Log("[" + this.Name + "] " + "Clicked finger Button's Tag: " + fingerNo);
            //            Console.WriteLine("Finger : " + fingerNo);

            // 指番号に対応するノートナンバーを取り出す
            int[] noteNums = Pianist.getNoteNumber(fingerNo, Pianist.LEFT_HAND);

            // クリックしたボタンに対応したシーンの再生をコントロールするためのボタンのTagを初期化
            this.noteNums = noteNums;
            this.repeatNoteButton.Tag = 0;
            this.nextNoteButton.Tag = 1;
            this.prevNoteButton.Tag = -1;

            // 指着目再生機能を使用中
            this.isFingerPlay = true;

            // クリックしたボタンに対応した最初のシーンを再生するメソッドを呼ぶ
            this.PlayKeystrokeScene(noteNums[0]);
*/
        }


        /// <summary>
        /// 指着目再生で次の音を再生するボタン
        /// 再生後、次の音がなければnextNoteButton.Tagを-1にする
        /// </summary>
        private void nextNoteButton_Click(object sender, RoutedEventArgs e)
        {
 /*           // 指着目再生機能を使用中でなければ脱出
            if (!this.isFingerPlay)
                return;

            int n = this.tag2Int(sender as Button);
            new Log("[" + this.Name + "] " + "nextNoteButton.Tag: " + n);
            if (n >= 0)
            {
                this.PlayKeystrokeScene(this.noteNums[n]);

                // 指着目再生の最後の音なら
                if (n+1 >= this.noteNums.Length)
                {
                    this.nextNoteButton.Tag = -1;
                }
                else
                {
                    this.nextNoteButton.Tag = n + 1;
                }
                this.repeatNoteButton.Tag = n;
                this.prevNoteButton.Tag = n - 1;

            }
            */
        }

        /// <summary>
        /// 指着目再生で前に再生したノートナンバーの音を再生する
        /// </summary>
        private void prevNoteButton_Click(object sender, RoutedEventArgs e)
        {
/*            // 指着目再生機能を使用中でなければ離脱
            if (!this.isFingerPlay)
                return;

            int n = this.tag2Int(sender as Button);
            new Log("[" + this.Name + "] " + "prevNoteButton.Tag: " + n);

            if (n >= 0)
            {
                this.PlayKeystrokeScene(this.noteNums[n]);

                // 指着目再生の最初の音なら
                if (n - 1 < 0)
                {
                    this.prevNoteButton.Tag = -1;
                }
                else
                {
                    this.prevNoteButton.Tag = n - 1;
                }
                this.nextNoteButton.Tag = n + 1;
                this.repeatNoteButton.Tag = n;

            }
            */
        }

        /// <summary>
        /// 指着目再生で同じ音を繰り返し再生するボタン
        /// </summary>
        private void repeatNoteButton_Click(object sender, RoutedEventArgs e)
        {
/*            // 指着目再生機能を使用中でなければ離脱
            if (!this.isFingerPlay)
                return;
                
            int n = this.tag2Int(sender as Button);
            new Log("[" + this.Name + "] " + "repeatNoteButton.Tag: " + n);
            this.PlayKeystrokeScene(this.noteNums[n]);
            */
        }

        #endregion

    }
}


