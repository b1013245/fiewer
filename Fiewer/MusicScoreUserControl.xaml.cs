﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualBasic.FileIO;



namespace Fiewer
{
    /// <summary>
    /// MusicScoreUserControl.xaml の相互作用ロジック
    /// 楽譜部分
    /// </summary>
    public partial class MusicScoreUserControl : UserControl
    {

        #region property
        private double MARGIN_RN = 2.0f; // 音符ボタン左上とインジケータ左上との間のマージン
        private int indicatorNo = 0;
        private BadFingeringHabits[] BadHabits { set; get; }
        private string IndicatorName = "indicator";
        #endregion




        public MusicScoreUserControl()
        {
            InitializeComponent();
         
        }



        /// <summary>
        /// 楽譜上の各音のボタンがクリックされたときに呼び出される
        /// MovieController.PlayKeystrokeScene(TimeSpan ts)を呼び出す
        /// </summary>
        /// <param name="sender">クリックしたボタン</param>
        private void note_Click(object sender, RoutedEventArgs e)
        {
            // 指着目再生機能を使用中なら不使用にする
            MainWindow win = (MainWindow)Application.Current.MainWindow;
            /*            if (win.movieController.isFingerPlay)
                            win.movieController.isFingerPlay = false;
                            */

            // クリックしたボタンのTagを取り出す
            int noteNo = new Calc().ButtonTag2Int(sender);
            new Log("NoteButton Clicked", "NoteButton's Tag", noteNo.ToString());

            // MovieControllerクラスのメソッドを呼ぶためにWindowを経由する
            win.movieController.playKeystrokeScene(noteNo);
            

        }

        /// <summary>
        /// 演奏箇所を示すインジケータを音符ボタン上に表示する
        /// </summary>
        /// <param name="noteNo">インジケータが示す音符のノートナンバー</param>
        public void moveIndicatorRectangle(int noteNo)
        {
            // noteNoと合致する音符ボタンを取得する
            var btn = this.FindName("note" + noteNo);
            Button button = (Button)btn;

            // Canvas左上を原点とした音符ボタンの左上座標
            Point pt_note = button.TranslatePoint(new Point(), this.canvas);            

            // 連続で同じ音を再生しないなら
            if(this.indicatorNo != noteNo)
            {                

                Rectangle rec = (Rectangle)this.FindName(this.IndicatorName);
                System.Console.WriteLine(rec.Name);
                Canvas.SetTop(rec, pt_note.Y - MARGIN_RN);
                Canvas.SetLeft(rec, pt_note.X - MARGIN_RN);
                this.indicatorNo = noteNo;

            }

        }

        /// <summary>
        /// インジケータの矩形を作る
        /// </summary>
        /// <returns>インジケータの矩形</returns>
        private Rectangle createIndicatorRectangle()
        {
            Rectangle rec = new Rectangle();
            rec.Width = 15;
            rec.Height = 37.683;
            rec.Stroke = Brushes.Red;
            rec.StrokeThickness = 1;
            rec.Name = this.IndicatorName;
            return rec;
        }



        #region Annotate Bad Fingering Note
        /// <summary>
        /// 悪癖が出現する音を楽譜上に四角形で表示する
        /// 1つの音符に0から4つの四角形を表示する
        //  悪癖と色、四角形の座標の対応は固定させる
        /// </summary>
        /// <param name="path">悪癖の確率CSVファイルへのフルパス</param>
        public void AnnotateNote(string path)
        {
            this.BadHabits = this.ReadProb(path);

            foreach(BadFingeringHabits bfh in this.BadHabits)
            {
                if(bfh == null)
                {// 
                    continue;
                }
                
                // Selectはループ回数と値を出してくれる
                foreach (var item in bfh.isAnnotate.Select((v, i) => new { Value = v, Index = i }))
                {
                    // 悪癖があるなら四角形を表示する
                    if (item.Value)
                    {
                        // 音番号と対応する音符ボタンを取得
                        Button btn = (Button)this.FindName("note" + bfh.NoteNumber);
                        // Canvas左上を原点とした音符ボタンの左上座標
                        double btn_x = btn.TranslatePoint(new Point(), this.canvas).X;
                        Rectangle rec = this.CreateAnnotateRect(item.Index);


                        // noteNumberによって段を判断する
                        int row = 0;
                        if (bfh.NoteNumber >= 37) row = 1;
                        if (bfh.NoteNumber >= 73) row = 2;
                        Rectangle r = (Rectangle)this.FindName("BFH" +row.ToString()+ item.Index);
                        double r_y = r.TranslatePoint(new Point(), this.canvas).Y;
                        Canvas.SetTop(rec, r_y);
                        Canvas.SetLeft(rec, btn_x - MARGIN_RN);
                        canvas.Children.Add(rec);
                    }
                }
                
               
            }

           
        }

        
        /// <summary>
        /// 引数に対応した悪癖用の四角形を返す
        /// </summary>
        /// <param name="num">悪癖の番号</param>
        private Rectangle CreateAnnotateRect(int num)
        {
            Rectangle rec = new Rectangle();
            rec.Width = 15;
            rec.Height = 15;
            rec.StrokeThickness = 0;
//            rec.Stroke = Brushes.Black;

            switch (num)
            {
                case 0:
                    rec.Fill = Brushes.LightPink;
                        break;
                case 1:
                    rec.Fill = Brushes.Plum;
                    break;
                case 2:
                    rec.Fill = Brushes.PaleTurquoise;
                    break;
                case 3:
                    rec.Fill = Brushes.LemonChiffon;
                    break;
            }

           
            return rec;
        }



        /// <summary>
        /// 悪癖の確率のCSVを読み込んで返す
        /// </summary>
        /// <param name="path">CSVファイルへのパス</param>
        /// <returns>CSV読み込んで値を格納した配列</returns>
        private BadFingeringHabits[] ReadProb(string path)
        {
            TextFieldParser parser = new TextFieldParser(path);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            BadFingeringHabits[] bfh = new BadFingeringHabits[97]; // 96音の曲の時

            while (!parser.EndOfData)
            {
                string[] cols = parser.ReadFields();
                // 1行目はnull, 2行目は列名なのでスキップする
                if (parser.LineNumber > 2)
                {
                    // 扱いやすいように添え字1から96のあいだで配列に要素を入れる
                    int i = Convert.ToInt32(cols[0]);

                    bfh[i] = new BadFingeringHabits(i, cols[1], cols[2], cols[3], cols[4]);


                }
                // 最後の行の時
                else if (parser.LineNumber == -1)
                {
                    bfh[bfh.Length - 1] = new BadFingeringHabits(bfh.Length - 1, cols[1], cols[2], cols[3], cols[4]);

                }

            }

            return bfh;
        }


        #endregion

    }
}
