﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Fiewer
{
    /// <summary>
    /// MovieWindow.xaml の相互作用ロジック
    /// 動画の再生を扱うウィンドウ
    /// </summary>
    public partial class MovieWindow : Window
    {
        #region Fields
//        internal readonly MediaElement[] mediaElementArray; // MediaElementオブジェクトの管理配列
        private Dictionary<string, MediaElement> mediaElementDic;
        private DispatcherTimer mediaElement_PauseTimer;
        
        internal readonly Pianist pianist;
        #endregion

        #region Public Fields
        public Dictionary<string, MediaElement> MediaElementDic { get { return this.mediaElementDic; } private set { this.mediaElementDic = value; }   }
        #endregion


        #region Constructor
        public MovieWindow(Dictionary<string, Uri> media)
        {
            InitializeComponent();
            this.MediaElementDic = new Dictionary<string, MediaElement>();

            // リソース設定
            foreach(KeyValuePair<string, Uri> pair in media)
            {
                if(pair.Key == Value.Front)
                {
                    this.mediaElement_front.Source = pair.Value;
                    this.MediaElementDic.Add(Value.Front, this.mediaElement_front);
                    continue;
                }
                if(pair.Key == Value.Left)
                {
                    this.mediaElement_left.Source = pair.Value;
                    this.MediaElementDic.Add(Value.Left, this.mediaElement_left);
                    continue;
                }
                if(pair.Key == Value.Right)
                {
                    this.mediaElement_right.Source = pair.Value;
                    this.MediaElementDic.Add(Value.Right, this.mediaElement_right);
                    continue;
                }
            }

            this.Title = media[Value.Midi].Segments[6] + media[Value.Midi].Segments[7];

            // MIDIへのパス指定
            this.pianist = new Pianist(media[Value.Midi].LocalPath);
        

            this.SetMediaElemetPauseTimer();
            this.Play();
            this.Pause();
        }
        #endregion

        #region Set MediaElement Size
        /// <summary>
        /// Windowサイズに合わせてMedia Elementのサイズを設定する
        /// </summary>
        private void SetMediaElementSize()
        {
            int count = this.CountShowedMediaElement(); // 表示中のMedia Elementの数
            int show_index = 0; // 今何番目のMediaElementのサイズを調整しているか, 0からカウント
            float m_h = 5;
            float m_w = 2;
            double w_w = this.RenderSize.Width;
            double w_h = this.RenderSize.Height;

            foreach (MediaElement me in this.MediaElementDic.Values)
            {
                if (me.IsVisible) { 
                    me.Width = (count != 1) ? w_w / 2 : w_w;
                    me.Height = (count != 1) ? w_h / 2 - m_h : w_h - m_h;


                    switch (count)
                    {
                        case 1:
                            Canvas.SetTop(me, 0);
                            Canvas.SetLeft(me, 0);
                            break;
                        case 2:
                            Canvas.SetTop(me, 0);
                            Canvas.SetLeft(me, me.Width * show_index);
                            show_index++;
                            break;
                        case 3:
                            if (show_index == 2)
                            {
                                Canvas.SetTop(me, me.Height + m_h);
                                Canvas.SetLeft(me, m_w);
                            }
                            else
                            {
                                Canvas.SetTop(me, 0);
                                Canvas.SetLeft(me, me.Width * show_index + m_w);
                               
                            }
                            show_index++;
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// ウィンドウサイズが変更されたら呼び出される
        /// ウィンドウを生成した時も呼び出される
        /// </summary>
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            new Log("MovieWindow's Size Changed", new string[] { "MovieWindow's title", "Width", "Height" }, new string[] { this.Title, this.RenderSize.Width.ToString(), this.RenderSize.Height.ToString() });
            this.SetMediaElementSize();

        }

        /// <summary>
        /// 表示中のMediaElementの個数を数えて返す
        /// </summary>
        /// <returns>表示中のMediaElementの個数</returns>
        private int CountShowedMediaElement()
        {
            int count = 0;

            foreach(MediaElement me in this.mediaElementDic.Values)
            {
                if (me.IsVisible)
                    count++;
            }

            return count;
        }

        #endregion


        #region Play One Note
        /// <summary>
        /// mediaElement_PauseTimerのセットアップ
        /// １回だけ呼び出す
        /// </summary>
        private void SetMediaElemetPauseTimer()
        {
            this.mediaElement_PauseTimer = new DispatcherTimer();
            this.mediaElement_PauseTimer.Tick += new EventHandler(mediaElement_PauseTimer_Tick);

        }


        /// <summary>
        /// 音ごとのシーンを再生する
        /// クリックした音の１音前から１音後までを再生する
        /// デバッグのため計算には常に正面動画の打鍵時刻を利用する
        /// </summary>
        /// <param name="noteNo">クリックした音が何音目か</param>
        public void PlayKeystrokeScene(int noteNo)
        {

            // ボタン連打時に備えて停止タイマーを止める
            this.mediaElement_PauseTimer.Stop();

            // クリックする１音前の音の打鍵再生時刻を取り出す
            int i = (noteNo > 1) ? noteNo - 1 : noteNo;
            Dictionary<string, float> f = new Dictionary<string, float>()
            {
                {Value.Front,  this.pianist.midis[i].touchPlayTimeFront},
                {Value.Left,  this.pianist.midis[i].touchPlayTimeLeft},
                {Value.Right, this.pianist.midis[i].touchPlayTimeRight }
            };
            Dictionary<string, TimeSpan> ts = new Dictionary<string, TimeSpan>();
            foreach (KeyValuePair<string, float> pair in f)
            {
                ts.Add(pair.Key, new TimeSpan(0, 0, 0, (int)Math.Truncate(pair.Value), calcMilliseconds(pair.Value)));
            }

            // 最後の音以外であれば
            if (noteNo < 96)
            {
                int j = noteNo + 1;
                // 次の打鍵の再生時刻
                float nf = this.pianist.midis[j].touchPlayTimeFront;
                // 一時停止するタイマーを起動させる時間を設定
                this.mediaElement_PauseTimer.Interval = this.calcSpan(nf, f[Value.Front], (float)this.MediaElementDic.Values.ToArray()[0].SpeedRatio);
            }
            // MusicScoreUserControlクラスのメソッドを呼ぶためにWindowを経由する
            MainWindow win = (MainWindow)Application.Current.MainWindow;
            win.musicScoreUserControl.moveIndicatorRectangle(noteNo);


            // 指定した再生時刻にジャンプする
            foreach (KeyValuePair<string, MediaElement> pair in this.MediaElementDic)
            {
                   pair.Value.Position = ts[pair.Key];
               
            }
            this.Play();
            if (noteNo < 96)
                this.mediaElement_PauseTimer.Start();

        }

        /// <summary>余剰計算でミリセカンドを求める</summary>
        /// <param name="sec">ミリセカンドを含む秒数</param>
        /// <returns name="ms">secのミリセカンド部分</returns>
        private int calcMilliseconds(float sec)
        {
            int ms = (int)((sec % 1) * 1000);
            return ms;
        }


        /// <summary>
        /// 次に打鍵するまでの時間を計算する
        /// </summary>
        /// <param name="next">次の音の打鍵時刻[sec]</param>
        /// <param name="current">１音前の打鍵時刻[sec]</param>
        /// <param name="ps">再生速度スライダーの値</param>
        /// <returns>再生速度を考慮した１音前の打鍵時刻と次の音の打鍵時刻との差</returns>
        private TimeSpan calcSpan(float next, float prev, float ps)
        {
            double s = (next - prev) * (1 / ps);
            // TODO: +0.250はMIDIと動画のずれ　どの動画でも同じ
            TimeSpan span = TimeSpan.FromSeconds(s +0.250);
            return span;
        }

        /// <summary>
        /// 1音のシーンの再生が終わったとき一時停止タイマーに呼ばれる
        /// 動画を一時停止する
        /// </summary>
        private void mediaElement_PauseTimer_Tick(object sender, EventArgs e)
        {
                this.Pause();
                this.mediaElement_PauseTimer.Stop();
        }
        #endregion

        #region Play All Notes
        /// <summary>
        /// 最初の音から最後の音までをノンストップで再生する
        /// </summary>
        public void playAllNote()
        {
            // タイマーが動いているなら止める
            this.mediaElement_PauseTimer.Stop();

            // 最初の打鍵の再生時刻を取り出す
            int i = 1;
            Dictionary<string, float> f = new Dictionary<string, float>()
            {
                {Value.Front,  this.pianist.midis[i].touchPlayTimeFront},
                {Value.Left,  this.pianist.midis[i].touchPlayTimeLeft},
                {Value.Right, this.pianist.midis[i].touchPlayTimeRight }
            };
            Dictionary<string, TimeSpan> ts = new Dictionary<string, TimeSpan>();
            foreach (KeyValuePair<string, float> pair in f)
            {
                ts.Add(pair.Key, new TimeSpan(0, 0, 0, (int)Math.Truncate(pair.Value), calcMilliseconds(pair.Value)));
            }

            // 指定した再生時刻にジャンプする
            foreach (KeyValuePair<string, MediaElement> pair in this.MediaElementDic)
            {
                pair.Value.Position = ts[pair.Key];
            }
            this.Play();

        }
        #endregion

        #region Play MediaElement
        /// <summary>
        /// ウィンドウ内の開いているすべてのMediaElementを再生する
        /// </summary>
        public void Play()
        {
            foreach(MediaElement me in this.MediaElementDic.Values)
            {
                me.Play();
            }
        }

        #endregion

        #region Play Movie From Continued
        public void PlayContinue()
        {
            // タイマーが動いているなら止める
            this.mediaElement_PauseTimer.Stop();

            foreach (MediaElement me in this.MediaElementDic.Values)
            {
                TimeSpan ts = me.Position;
                me.Play();
                me.Position = ts;
            }
        }
        #endregion

        #region Pause Media Element
        public void Pause()
        {
            foreach(MediaElement me in this.MediaElementDic.Values)
            {
                me.Pause();
            }
        }

        #endregion

        #region Change Speed Ratio
        /// <summary>
        /// 再生速度を変更する
        /// </summary>
        /// <param name="speedRatio">計算済みの再生速度</param>
        public void changeMediaElementSpeedRatio(double speedRatio)
        {
            foreach(MediaElement me in this.MediaElementDic.Values)
            {
                me.SpeedRatio = speedRatio;
            }
        }

        #endregion

        #region Frame by Frame

        /// <summary>
        /// コマ戻るボタンがクリックされたら動画の再生時刻を25msec戻す
        /// コマ進むボタンがクリックされたら動画の再生時刻を25msec進める
        /// </summary>
        /// <param name="tag">戻る/進むコマ送りボタンのTag</param>
        public void FramebyFrame(int tag)
        {
            int t = (tag < 0) ? -25 : 25;

            foreach (MediaElement me in this.MediaElementDic.Values)
            {
                me.Position += new TimeSpan(0, 0, 0, 0, t);

            }
        }

        #endregion

        #region File Open and Failure
        
        private void mediaElementMediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
//            System.Console.WriteLine("Media Failed:" + e.ErrorException);
            new Log("File Open Failed","Error", e.ErrorException.ToString());
        }

        private void mediaElementMediaOpened(object sender, RoutedEventArgs e)
        {

            MainWindow mw = (MainWindow)Application.Current.MainWindow;
            if (mw.movieController.seekSlider.Maximum == 10)
                mw.movieController.SetSeekSlider();
            this.Play();
            this.Pause();

        }

        #endregion

        #region Close Window
        /// <summary>
        /// ウィンドウを閉じるときに呼び出される
        /// </summary>
        private void movieWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            new Log("MovieWindow Closed", "MovieWindow's Title", this.Title);
            MainWindow win = (MainWindow)Application.Current.MainWindow;
            win.removeMovieWindow((int)this.Tag);

        }
        #endregion

        #region Show and Hide MediaElement
        /// <summary>
        /// MediaElementを表示する
        /// 表示しているMediaElementの個数に合わせてMediaElementのサイズを変える
        /// </summary>
        /// <param name="key">表示するMediaElementを格納しているmediaElementDicのkey</param>
        public void ShowMediaElement(string key)
        {
            this.mediaElementDic[key].Visibility = Visibility.Visible;
            this.SetMediaElementSize();
        }

        /// <summary>
        /// MediaElementを非表示にする
        /// 表示しているMediaElementの個数に合わせてMediaElementのサイズを変える
        /// </summary>
        /// <param name="key"> 非表示にするMediaElementを格納しているmediaElementDicのkey</param>
        public void HideMediaElement(string key)
        {
            this.mediaElementDic[key].Visibility = Visibility.Hidden;
            this.SetMediaElementSize();
        }

        #endregion

    }
}
