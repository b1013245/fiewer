﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fiewer
{
    // 配列にして扱うの前提で作った
    class BadFingeringHabits
    {
        public int? NoteNumber;
        public string Lifting;
        public string Contact;
        public string Curl;
        public string Slanting;
        public bool[] isAnnotate;

        public BadFingeringHabits(int number, string lifting, string contact, string curl, string slanting)
        {
            this.NoteNumber = number;
            this.Lifting = lifting;
            this.Contact = contact;
            this.Curl = curl;
            this.Slanting = slanting;
            this.isAnnotate = new bool[] {isOver80(lifting), isOver80(contact), isOver80(curl), isOver80(slanting) }; 
        }

        /// <summary>
        /// 悪癖の確率が80%以上ならtrue, 80％未満ならfalseを返す
        /// </summary>
        /// <param name="str">悪癖の確率の文字列、"89%"など'%'がつく形</param>
        private bool isOver80(string str)
        {
            int a = Convert.ToInt32(str.Split('%')[0]);
            bool isOver = (a >= 80) ? true : false;
            return isOver;
        }


    }
}
