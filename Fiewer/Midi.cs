﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fiewer
{
    class Midi
    {
        #region Fields Property
        public int noteNo { get; }
        public string pitch { get; }
        public float touchPlayTimeFront { get; }
        public float touchPlayTimeLeft { get; }
        public float touchPlayTimeRight { get; }
        public float Sub_FL { get; }
        public float Sub_FR { get; }
        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="noteNo">楽譜上で何番目の音か</param>
        /// <param name="pitch">音高</param>
        /// <param name="touchPlayTimeFront">正面動画の打鍵再生時刻k</param>
        /// <param name="touchPlayTimeLeft">左動画の打鍵再生時刻</param>
        /// <param name="touchPlayTimeRight">右動画の打鍵再生時刻</param>
        public Midi(int noteNo, string pitch, float touchPlayTimeFront, float touchPlayTimeLeft, float touchPlayTimeRight)
            {
                this.noteNo = noteNo;
                this.pitch = pitch;
                this.touchPlayTimeFront = touchPlayTimeFront;
                this.touchPlayTimeLeft = touchPlayTimeLeft;
                this.touchPlayTimeRight = touchPlayTimeRight;
                this.Sub_FL = touchPlayTimeLeft - touchPlayTimeFront;
                this.Sub_FR = touchPlayTimeRight - touchPlayTimeFront;
            }

    }
}
