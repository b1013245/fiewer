﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;


namespace Fiewer
{
    public class SingleMediaElement: MediaElement
    {

        #region Constuctor
        public SingleMediaElement(Uri uri, string tag)
        {
            this.Tag = tag;
            this.LoadedBehavior = MediaState.Manual;
            this.IsMuted = true;
            this.ScrubbingEnabled = true;
            
        }
        #endregion
    }
}
