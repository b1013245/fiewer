﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fiewer
{
    /// <summary>
    /// ピアニストの演奏データ（動画, MIDI, 指の動き）を扱うクラス
    /// </summary>

    class Pianist
    {

        #region Private Fields
        // read-onlyのMIDIデータ構造体
        public Midi[] midis { get; }
        // MIDIのノートの個数の最大値
        private const int NOTE_MAX = 98;

        // 各指で弾くノートナンバー
        static private int[] c5 = new int[] { 2, 6, 8, 12, 14, 18, 20, 24, 74, 78, 80, 84 };
        static private int[] d5 = new int[] { 3, 5, 9, 11, 15, 17, 21, 23, 26, 30, 32, 36, 38, 42, 44, 48, 62, 66, 68, 72, 75, 77, 81, 83, 85, 87, 89, 91, 94 };
        static private int[] e5 = new int[] { 4,  10,  16,  22,  27,  29,  33,  35,  39,  41,  45,  47,  50,  54,  56,  60,  63,  65,  69,  71,  73,  76,  79,  82,  86,  88,  90,  93,  95 };
        static private int[] f5 = new int[] { 28,  34,  40,  46,  51,  53,  57,  59,  61,  64,  67,  70,  92,  96 };
        static private int[] g5 = new int[] { 1, 7, 13, 19, 25, 31, 37, 43, 49, 52, 55, 58 };

        // read-only ある音の悪癖の所属確率
       // public BadFingeringHabits[] BadHabits { get; } 

        #endregion

        #region Public fields

        // ファイルの種類を識別する定数
        public const int MOVIE = 1;
        public const int MIDI = 2;
        public const int FINGERING = 3;

        // どちらの手かを識別するための定数
        public const int RIGHT_HAND = 1;
        public const int LEFT_HAND = 2;

        #endregion


        #region Constructor
        //// <summary>
        /// コンストラクタ
        /// </summary>
        public Pianist(string midi_path)
        {
            // MIDIデータを読み込んで保持する
            this.midis = ReadMidi(midi_path);

        }
        #endregion


        /// <summary>
        /// MIDIデータのテキストファイルを読み込み、配列として返す
        /// </summary>
        /// <param path>MIDIデータのテキストファイルへのフルパス</param>
        /// <returns>MIDIクラスのインスタンスを要素として持つ配列</returns>
        private Midi[] ReadMidi(string path)
        {
            // MIDIクラスのインスタンスを格納する配列
            Midi[] m;

            // 1行分のMIDIデータ
            string line;

            // 読み込んでいる行番号(最初の行は0)
            int lineNo = 0; 

            // UTF-8でファイルを読み込む
            System.IO.StreamReader file = new System.IO.StreamReader(@path, System.Text.Encoding.GetEncoding("UTF-8"));
            m = new Midi[NOTE_MAX];

            // 最後の行まで1行ずつ読み込む
            while ((line = file.ReadLine()) != null)
            {
                // Console.WriteLine(line);

                // 1行に含まれる要素をタブで区切る
                string[] s = line.Split('\t');
                // 最初の行（列名）でなければ
                if(lineNo != 0)
                {
                    m[lineNo] = new Midi(int.Parse(s[0]), s[1], float.Parse(s[2]), float.Parse(s[3]), float.Parse(s[4]));
                }else
                {
                    m[lineNo] = new Midi(-1, "PITCH", -1.0f, -1.0f, -1.0f);
                }

                lineNo++;
            }

            // ファイルを閉じる
            file.Close();

            return m;
        }

        




        /// <summary>
        /// 各指に対応したMIDIのノートナンバーを返す
        /// C.Czerny30番練習曲1番のみに対応
        /// （例：入力が小指なら、出力は1, 7, ...）
        /// </summary>
        /// <param name="fingerNo">指番号</param>
        /// <param name="whichHand">どちらの手か</param>
        /// <returns>MIDIのノートナンバーのint型配列</returns>
        static public int[] getNoteNumber(int fingerNo, int whichHand)
        {

            switch (fingerNo)
            {
                case 1:
                    return (whichHand == RIGHT_HAND) ? c5 : g5;
                case 2:
                    return (whichHand == RIGHT_HAND) ? d5 : f5;
                case 3:
                    return e5;
                case 4:
                    return (whichHand == RIGHT_HAND) ? f5 : d5;
                case 5:
                    return (whichHand == RIGHT_HAND) ? g5 : c5;
                default:
                    Console.WriteLine("[Func:getNoteNumber in Pianist Class] Not found finger number.");
                    break;
            }
            return (new int[99]);
        }







    }

}
