﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Fiewer
{
    /// <summary>
    /// テキストファイルにログを出力する操作を持つクラス
    /// </summary>
    class Log
    {
        #region Private Fields
        private string baseFileName = "log.txt"; // 出力ファイルに絶対つける部分名
        private string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "//FiewerLog/";
        private string[] month = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        #endregion

        #region Public Fields
        public string BaseFileName
        {
            get { return this.baseFileName; }
        }
        public string FolderPath
        {
            get { return folderPath; }
        }
        public string[] Month
        {
            get { return this.month; }
        }
        public Dictionary<string, string> Values;
        #endregion

        #region Constructor
        /// <summary>
        /// 日時と操作を出力するとき
        /// </summary>
        /// <param name="str">操作</param>
        public Log(string str)
        {
            this.Write(str);

        }

        /// <summary>
        /// 一行で操作と値を出力するとき
        /// </summary>
        /// <param name="str">操作</param>
        /// <param name="label">値のラベル</param>
        /// <param name="value">値</param>
        public Log(string str, string label, string value)
        {
            this.Write(str + "," + label + ":" + value + ",");
        }

        /// <summary>
        /// 日時と操作と複数の値（ボタンのTagなど）を出力するとき
        /// labelsとvaluesの添え字は同じにする（辞書的な）
        /// </summary>
        /// <param name="str">操作</param>
        /// <param name="valueNames">値のラベル</param>
        /// <param name="dic">出力する値</param>
        public Log(string str, string[] labels, string[] values)
        {
            if (labels.Length != values.Length)
            {
                this.Write("Write Log Error----Not Equal Labels and Values Count !!");
                return;
            }
                
            string line = str + ",";
            for(int i=0; i<labels.Length; i++)
            {
                line += labels[i] + ":" + values[i] + ",";
            }

            this.Write(line);
        }

        /// <summary>
        /// 日時と操作と複数の値（ボタンのTagなど）を出力するとき
        /// Valuesに値を一時保存してから出力するとき
        /// </summary>
        /// <param name="isValue">値があるか</param
        public Log(bool isValue)
        {
            this.Values = new Dictionary<string, string>();
        }
        #endregion


        #region Write Log
        /// <summary>
        /// 操作ログをテキストファイルに書き込む
        /// </summary>
        /// <param name="str">書き込む文字列</param>
        public void Write(string str)
        {
            DateTime dt = DateTime.Now;
            string fpath = this.FolderPath + this.Month[dt.Month - 1] + dt.Day + this.BaseFileName;

            // ログを格納するフォルダがなければ作成する
            if (!Directory.Exists(this.FolderPath))
                Directory.CreateDirectory(this.FolderPath);

            using (StreamWriter writer = new StreamWriter(fpath, true, Encoding.GetEncoding("utf-8")))
            {
                writer.WriteLine(dt.ToString("G") + "," + str);
            }

        }

        /// <summary>
        /// Dictionary使って複数のラベルと値を書き込むとき
        /// </summary>
        /// <param name="str">操作</param>
        /// <param name="dic">ラベルと値の辞書</param>
        public void Write(string str, Dictionary<string, string> dic)
        {
            string line = str + ",";
            foreach (KeyValuePair<string, string> pair in dic)
            {
                line += pair.Key + ":" + pair.Value + ",";
            }
            Write(line);
        }

        #endregion

    }
}
