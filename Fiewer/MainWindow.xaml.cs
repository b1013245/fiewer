﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fiewer
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Fields
        private List<MovieWindow> MovieWindowList = new List<MovieWindow>();
        public List<MovieWindow> getMovieWindowList()
        {
            return this.MovieWindowList;
        }
        private int MovieWindowCount = 0;
        private bool isExpMode = false; // 実験時はtrue
        
        #endregion

        #region Constructor
        public MainWindow()
        {
            new Log("--------------------------Set up.--------------------------");

            InitializeComponent();
            this.movieController.MovieWindowList = this.getMovieWindowList();

            new Log("----------------------------------------------------");
            if (this.isExpMode)
                this.movieController.isExpMode = true;
        }

        #endregion



        #region File Select Button

        private void fileSelectButton_Click(object sender, RoutedEventArgs e)
        {
            new Log("FileSelectButton Clicked");

            // ファイル選択のためのダイアログ
            Microsoft.Win32.OpenFileDialog openPicker = new Microsoft.Win32.OpenFileDialog();
            // 選択できるファイルのフィルタ
            openPicker.Filter = "Text Files| *.txt";

            // ダイアログが開かれたら
            if (openPicker.ShowDialog() == true)
            {
                Dictionary<string, Uri> media = new Dictionary<string, Uri>();

                media.Add(Value.Midi, new Uri(openPicker.FileName, UriKind.Absolute));
                media.Add(Value.Front, new Uri(openPicker.FileName.Replace("MIDI.txt", "F.mp4"), UriKind.Absolute));
                media.Add(Value.Left, new Uri(openPicker.FileName.Replace("MIDI.txt", "L.mp4"), UriKind.Absolute));
                media.Add(Value.Right, new Uri(openPicker.FileName.Replace("MIDI.txt", "R.mp4"), UriKind.Absolute));
                //media.Add(Value.Predict, new Uri(openPicker.FileName.Replace("MIDI.txt", "prob.csv"), UriKind.Absolute));

                // 悪癖出現個所をアノテーション
                this.musicScoreUserControl.AnnotateNote(new Uri(openPicker.FileName.Replace("MIDI.txt", "prob.csv"), UriKind.Absolute).LocalPath);

                string str = "";
                foreach(Uri uri in media.Values)
                {
                    str += uri.Segments[6] + uri.Segments[7] + ",";
                }
                new Log("File Selected", "File Path",str);
                this.ShowMovieWindow(media);

                
                // 画像ファイルの探索
                Dictionary<string, Uri> path = new Dictionary<string, Uri>();
                path.Add("Heatmap", new Uri(openPicker.FileName.Replace("MIDI.txt", "hmap.png"), UriKind.Absolute));
                //path.Add("Mocap", new Uri(openPicker.FileName.Replace("MIDI.txt", "Mocap.png"), UriKind.Absolute));
                path.Add("IOI", this.getUri(openPicker.FileName, int.Parse(Value.IOI_Tag)));
                path.Add("Velocity", this.getUri(openPicker.FileName, int.Parse(Value.Velocity_Tag)));
                this.ShowDataWindow(path);
                
            } 
            else
            {
                // ファイルを選択せずにダイアログが閉じられたら
                new Log(" Canceled file select");
            }
       
    }


        /// <summary>
        /// 画像ファイル名の一部からUriを作って返す
        /// </summary>
        /// <param name="file">選択されたファイルのフルパス</param>
        /// <param name="target">IOIとベロシティどっちのUri作るか（値はValue.resx参照）</param>
        /// <returns>ファイルパス</returns>
        private Uri getUri(string file, int target)
        {
            string str = (target == int.Parse(Value.IOI_Tag)) ? "IOI.png" : "Velocity.png";
            int i = file.LastIndexOf("-");
            string s = file.Substring(0, i);
            Uri uri = new Uri(s + str);
            return uri;
        }
        




        #endregion

        #region MovieWindow Control
        private void ShowMovieWindow(Dictionary<string,Uri> media)
        {
            MovieWindow mw = new MovieWindow(media);
            mw.Tag = this.MovieWindowCount++;
            this.MovieWindowList.Add(mw);
            mw.Show();
            this.movieController.SetSeekSliderTimer();

            if (this.MovieWindowList.Count == 1)
            {
                // MEのうち開いたものをどれか一つミュート解除
                foreach (MediaElement me in this.MovieWindowList[0].MediaElementDic.Values)
                {
                    if (me.IsVisible)
                    {
                        me.IsMuted = false;
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
                
            }
            
            mw.changeMediaElementSpeedRatio(this.movieController.playSpeedSlider.Value/100);

        }

        public void removeMovieWindow(int mwTag)
        {
            MovieWindow rmw = null;

            foreach (MovieWindow mw in this.MovieWindowList)
            {
                if ((int)mw.Tag == mwTag)
                    rmw = mw;
            }
            this.MovieWindowList.Remove(rmw);

            if(this.MovieWindowList.Count > 0)
            {
                bool isSilent = true; // IsMuted探索フラグ
                MediaElement next = null; // 次に音を出すME
                foreach(MovieWindow mw in this.MovieWindowList)
                {
                    foreach (MediaElement me in mw.MediaElementDic.Values)
                    {
                        if (me.IsMuted == false)
                            isSilent = false;
                        if (me.Source != null)
                            next = me;
                    }
                }
                // 音を出すMEがなくなったら
                if (isSilent && next != null)
                    next.IsMuted = false;
            }
        }

        #endregion




        #region DataWindow Control
        private void ShowDataWindow(Dictionary<string, Uri>path)
        {
            DataWindow dw = new DataWindow(path);
            dw.Show();
        }


        #endregion

        #region Play Button
        /// <summary>
        ///　演奏を１音目から最後まで再生する
        /// </summary>
        private void playButton_Click(object sender, RoutedEventArgs e)
       {
            new Log("playButton Clicked");
            foreach (MovieWindow mw in this.MovieWindowList)
            {
                mw.playAllNote();  
            }
        }
        #endregion

    }
}
