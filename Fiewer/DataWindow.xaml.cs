﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fiewer
{
    /// <summary>
    /// DataWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class DataWindow : Window
    {
        #region Private Fields
        private Dictionary<string, Image> DataDictionary { get; }
        #endregion

        /// <param name="path">keyはXAMLで定義した各Imageの名前、valueはパス</param>
        public DataWindow(Dictionary<string, Uri> path)
        {
            InitializeComponent();

            this.DataDictionary = new Dictionary<string, Image>();
            foreach(KeyValuePair<string, Uri> kv in path)
            {
                this.DataDictionary.Add(kv.Key, this.SetSource(kv.Key, kv.Value));
            }


        }

        #region Set Data
        /// <summary>
        /// 演奏情報を表示するImageに画像ファイル設定して返す
        /// </summary>
        /// <param name="name">Xamlで定義した名前</param>
        /// <param name="uri">表示するファイルのパス</param>
        /// <returns></returns>
        private Image SetSource(string name, Uri uri)
        {
            Image img = this.FindName(name) as Image;
            img.Source = new BitmapImage(uri);
            return img;
        }

        public void SetDataPosition()
        {

        }

        #endregion


        #region Show and Hide Data
        public void ShowData(string tag)
        {
            this.DataDictionary[tag].Visibility = Visibility.Visible;
        }

        public void HideData(string tag)
        {
            this.DataDictionary[tag].Visibility = Visibility.Hidden;
        }


        #endregion

        private void DataWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            new Log("DataWindow Closed");

        }
    }
}
